#include <shims.h>
#include <i2c_slave.h>
//#include <pot/ssd1306.h>
#include "main.h"

extern void Error_Handler();
extern I2C_HandleTypeDef hi2c1; // iic_mst
extern I2C_HandleTypeDef hi2c3; // iic_slv
//extern I2C_HandleTypeDef iic_mst;
//extern I2C_HandleTypeDef iic_slv;

uint8_t i2c_slv_data = 42;
int nrxcplt = 0; //number of rxcompletes



void HAL_I2C_ListenCpltCallback (I2C_HandleTypeDef *hi2c)
{
	//if(hi2c !=  &hi2c1) return;
	HAL_I2C_EnableListen_IT(hi2c);
}

void HAL_I2C_AddrCallback(I2C_HandleTypeDef *hi2c, uint8_t TransferDirection, uint16_t AddrMatchCode)
{
	//if(hi2c !=  &hi2c1) return;

	if(TransferDirection == I2C_DIRECTION_TRANSMIT)  // if the master wants to transmit the data
	{
		HAL_I2C_Slave_Seq_Receive_IT(hi2c, (uint8_t *) &i2c_slv_data, 1, I2C_FIRST_AND_LAST_FRAME);
	}
	else if(TransferDirection == I2C_DIRECTION_RECEIVE)
	{
		// master wants to receive
		HAL_I2C_Slave_Seq_Transmit_IT(hi2c, (uint8_t *) &i2c_slv_data, 1, I2C_FIRST_AND_LAST_FRAME);

		//HAL_I2C_Slave_Seq_Transmit_IT
	}
	else  // master requesting the data is not supported yet
	{
		Error_Handler();
	}
}

void HAL_I2C_SlaveRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	//if(hi2c !=  &hi2c1) return;
	//count++;
	HAL_I2C_EnableListen_IT(hi2c);
	nrxcplt++;
}

void HAL_I2C_SlaveTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	//if(hi2c !=  &hi2c1) return;
	//count++;
	HAL_I2C_EnableListen_IT(hi2c);
}


void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hi2c)
{
	//if(hi2c !=  &hi2c1) return;
	HAL_I2C_EnableListen_IT(hi2c);
}

void i2c_slave_init(I2C_HandleTypeDef *hi2c)
{
	HAL_I2C_EnableListen_IT(hi2c);

}


volatile int do_transmit = 0;
void systick_callback()
{
	if(HAL_GetTick() % 1000 == 0)
		do_transmit = 1;
}

void i2c_slv_test_loop() {

	if (HAL_I2C_EnableListen_IT(&iic_slv) != HAL_OK)
		Error_Handler();

	HAL_Delay(10);
	ssd1306_init_display(64, (uint32_t) &iic_mst);
	ssd1306_show_scr();

	uint32_t count = 0;
	uint8_t DevAddress = iic_slv.Instance->OAR1; // device addr of slave, (shifted)
	(void) DevAddress;

	while (1) {
		ssd1306_home();
		clear_scr();

		if(do_transmit) {
			do_transmit = 0;
			count++;
			HAL_I2C_Master_Transmit(&iic_mst, DevAddress, (uint8_t*) &count, 1, 100); // see gotcha 1
		}
		uint8_t rx = 13;
		(void)rx;
		//HAL_I2C_Master_Receive(&iic_mst, DevAddress, &rx, 1, 1000);
		ssd1306_printf("DAT %d\n", i2c_slv_data);
		ssd1306_printf("NC  %d  \n",  nrxcplt);
		ssd1306_show_scr();

		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);
		HAL_Delay(300);
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);
		HAL_Delay(300);
	}
}


