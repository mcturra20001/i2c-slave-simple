#if 0


// listen complete
// replaces default weak version
static volatile int cplt = 0;
void HAL_I2C_ListenCpltCallback(I2C_HandleTypeDef* hi2c) {
	cplt++;
    HAL_I2C_EnableListen_IT(hi2c); // put I2C back into listen mode
}


// doesn't seem to be called
static int err_cb = 0;
static int errorcode = 0;
void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hiic)
{
	err_cb++;
	errorcode = HAL_I2C_GetError(hiic);
	switch (errorcode) {
	case HAL_I2C_ERROR_AF:
		 __HAL_I2C_CLEAR_FLAG(hiic, I2C_FLAG_AF);
		 break;
	default:

		Error_Handler();
	}

	HAL_I2C_EnableListen_IT(hiic); // put I2C back into listen mode
}


//#define SID 0x4 // slave address (that's us)
static uint8_t stored_value = 0x42; // a value that we can read and write

volatile static int stcc = 0;
void HAL_I2C_SlaveTxCpltCallback(I2C_HandleTypeDef *hiic)
{
	stcc++;
	uint8_t dummy;
	HAL_I2C_Slave_Sequential_Transmit_IT(hiic, &dummy, 1, I2C_NEXT_FRAME);
}

void HAL_I2C_SlaveRxCpltCallback(I2C_HandleTypeDef *hiic)
{
	stcc++;
	uint8_t dummy;
	HAL_I2C_Slave_Sequential_Receive_IT(hiic, &dummy, 1, I2C_NEXT_FRAME);
}

volatile static int addr_call = 0;
// replaces "weak" version
void HAL_I2C_AddrCallback(I2C_HandleTypeDef *hiic, uint8_t TransferDirection, uint16_t AddrMatchCode)
{


	addr_call++;
	HAL_StatusTypeDef status;
  //if(AddrMatchCode != (SID <<1)) Error_Handler();
  if (TransferDirection == I2C_DIRECTION_RECEIVE) {
	  // Master wants to receive, so we must send
	  // i2cget -y 1 0x04 from Pi
	  //status = HAL_I2C_Slave_Sequential_Transmit_IT(hi2c, &stored_value, 1, I2C_FIRST_AND_LAST_FRAME);
	  status = HAL_I2C_Slave_Sequential_Transmit_IT(hiic, &stored_value, 1, I2C_FIRST_FRAME);
	  //status = HAL_I2C_Slave_Sequential_Transmit_IT(hi2c, &stored_value, 1, I2C_FIRST_AND_NEXT_FRAME);
	  //status = HAL_I2C_Slave_Transmit_IT(hi2c, &stored_value, 1); // possibly buggy??
	  if(status!= HAL_OK) Error_Handler();

  } else {
	  // master transmits, we receive
	  // try i2cset -y 1 0x04 0x23 from Pi
	  // then i2cget -y 1 0x04 should return 0x23
	  status = HAL_I2C_Slave_Sequential_Receive_IT(hiic, &stored_value, 1, I2C_FIRST_FRAME);
	  //status = HAL_I2C_Slave_Receive_IT(hi2c, &stored_value, 1);
	  if(status!= HAL_OK) Error_Handler();
  }
}

#endif
