# I2C Simple slave


This project contains a simple slave on I2C3.

The device address is 0x4. 


Typical pinouts:
```
         THIS F401     PICO    NUCLEO
         ----------    -----   ------
         I2C1  I2C3    I2C0    I2C3 (?)
         MST   SLV     MST     MST
🟡 SCL   PB6   PA8     5       D6/PB10
⚪ SDA   PB7   PB4     4       D3/PB3
```

SLV = slave, MST = master.

You can access the slave externally, or you can operate the device in loopback mode. Just connect the 2 SCL's and SDA's together with wires.


## Gotchas

1. Master can hang if slave is plugged in after master. So master may want to reset itself under such circumstances:
```
	HAL_StatusTypeDef status = HAL_I2C_Master_Transmit(&iic_mst, DevAddress, (uint8_t*)&count, 1, 1000);
	if(status != HAL_OK) NVIC_SystemReset();
```

2. Add 5k pull-up resistors  to the F401 slave SCL (PA8) and SDA (PB4). 
I did see an option in CubeMX that suggested that pull-ups could be configured for the I2C slave, but I couldn't find that on further examination.


## References:

* zet 3a

* 19d18 provides a suitable board

* [Youtube playlist](https://www.youtube.com/watch?v=X7WZQka2FHQ&list=PLfIJKC1ud8gj_P7Qb28aTr0t92uk_vwg0)

## Status

*	2024-07-24	Updated. Works. F401

*   2023-08-14  Started. Works
